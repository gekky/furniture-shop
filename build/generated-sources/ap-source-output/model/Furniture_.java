package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Ordered;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-06-03T01:46:45")
@StaticMetamodel(Furniture.class)
public class Furniture_ { 

    public static volatile SingularAttribute<Furniture, Double> price;
    public static volatile SingularAttribute<Furniture, String> description;
    public static volatile SingularAttribute<Furniture, String> name;
    public static volatile SingularAttribute<Furniture, Long> furnitureID;
    public static volatile CollectionAttribute<Furniture, Ordered> orders;
    public static volatile SingularAttribute<Furniture, String> fotourl;

}