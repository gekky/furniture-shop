package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-06-03T01:46:45")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, String> username;
    public static volatile SingularAttribute<User, String> description;
    public static volatile SingularAttribute<User, String> surname;
    public static volatile SingularAttribute<User, String> firstname;
    public static volatile SingularAttribute<User, Boolean> validated;
    public static volatile SingularAttribute<User, String> password;

}