package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Furniture;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-06-03T01:46:45")
@StaticMetamodel(Ordered.class)
public class Ordered_ { 

    public static volatile SingularAttribute<Ordered, Long> id;
    public static volatile CollectionAttribute<Ordered, Furniture> furniture;

}