/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import business.FurnitureFacade;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import model.Furniture;
/**
 * Furniture view bean
 * @author jonny
 */
@Named
@ManagedBean
@RequestScoped
public class AddFurnitureBean {
    /**
     * Furniture facade 
     * Injected EJB.
     */
    @EJB
    FurnitureFacade furniturefacade;
    /**
     * Name of furniture
     */
    protected String name;
    /**
     * Price of furniture
     */
    protected double price;
    /**
     * Description of furniture
     */
    protected String description;
    /**
     * Photo url
     */
    protected Part fotourl;

    public Part getFotourl() {
        return fotourl;
    }

    public void setFotourl(Part fotourl) {
        this.fotourl = fotourl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    
    public AddFurnitureBean() {
    }
    /**
     * Saving furniture
     * @return index
     */
    public String store(){
        String fName = (long)(new Random()).nextInt() + fotourl.getSubmittedFileName();
        String filename = "/home/jonny/NetBeansProjects/SemestralkaEJA-nabytko-shop/web/foto/" + fName;
        File f = new File(filename);
        try {
            ByteStreams.copy(fotourl.getInputStream(), new FileOutputStream(f));
        } catch (IOException ex) {
            Logger.getLogger(AddFurnitureBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        furniturefacade.createFurniture(new Furniture(name, price, description, "foto/"+fName));
        return "index?faces-redirect=true";
    }
    
    
}
