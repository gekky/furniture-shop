/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import business.UsersFacade;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import model.User;

/**
 *
 * @author jonny
 */
@Named
@ManagedBean
@SessionScoped
public class UserBean {
    
    @EJB
    UsersFacade usersfacade;
    
    private boolean isAnyUser;

    public UserBean() {   
    }
    /**
     * Searching for all users
     */
    @PostConstruct
    public void init(){
        Collection<User> users =  usersfacade.findAllUsers();
        isAnyUser = users.isEmpty();
    }
    /**
     * Any user exist ?
     * @return true|false
     */
    public boolean isIsAnyUser() {
        return isAnyUser;
    }

    public void setIsAnyUser(boolean isAnyUser) {
        this.isAnyUser = isAnyUser;
    }
    
    
    
}
