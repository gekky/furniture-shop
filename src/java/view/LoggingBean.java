/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import business.UsersFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import model.User;

/**
 * Bean for login of user
 * @author jonny
 */
@Named
@ManagedBean
@SessionScoped
public class LoggingBean implements Serializable{
    
    @EJB
    UsersFacade usersfacade;
    /**
     * Username
     */
    String username;
    /**
     * Password
     */
    String password;
    /**
     * Is logged variable in whole session
     */
    boolean isLogged;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIsLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }
   

    public LoggingBean() {
    }
    /**
     * Login which provides check of username and password credentials
     * @return 
     */
    public String login(){
        User u = usersfacade.findUser(username);
        if(u != null && !u.getPassword().equals(password)){
            isLogged = false;
        }
        else{
            isLogged = true;
        }
        return "index?faces-redirect=true";
    }
    /**
     * Check if is user logged
     * @return 
     */
    public boolean getIsLogged(){
        return isLogged;
    }
    /**
     * Logout which provides logout of user
     * @return 
     */
    public String loggout(){
        isLogged = false;
        return "index?faces-redirect=true";
    }
    
    
}
