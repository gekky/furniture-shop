/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import business.FurnitureFacade;
import business.OrderFacade;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import model.Furniture;
import model.Ordered;

/**
 * Bean which is handling orders.
 * @author jonny
 */
@Named
@ManagedBean
@SessionScoped
public class OrderBean {

    @EJB
            /**
             * Orders facade
             */
    OrderFacade orders;
    /**
     * Furniture facade provider of DAO
     */
    @EJB
    FurnitureFacade furniturefacade;
    
    Ordered order;
    
    private double price = 0;
    
    public OrderBean() {
        order = new Ordered();
    }
    
    public void addFurniture(){
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Furniture f = furniturefacade.find(params.get("furnitureID"));
        order.addFurniture(f);
    }
    
    public double getPrice(){
        price = 0.0;
        if(order != null && !order.getFurniture().isEmpty()){
            for(Furniture f : order.getFurniture()){
                price += f.getPrice();
            }
        }
        return price;
    }

    public Ordered getOrder() {
        return order;
    }

    public void setOrder(Ordered order) {
        this.order = order;
    }

    public String clear(){
        order.clearFurniture();
        return "index?faces-redirect=true";
    }
    
    public String store(){
        orders.create(order);
        return "Sucess";
    }
    
}
