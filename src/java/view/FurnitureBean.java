/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import business.FurnitureFacade;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.Furniture;

/**
 *  Furniture bean
 *  Uses furniture
 * @author Jan Tlamicha
 */
@Named
@ManagedBean
@RequestScoped
public class FurnitureBean implements Serializable{
    /**
     * Furniture facade
     */
    @EJB
    FurnitureFacade furniturefacade;

    public FurnitureBean() {
    }
    
    public Collection<Furniture> getFurniture() {
        return furniturefacade.findAll();
    }

    
    
    
}
