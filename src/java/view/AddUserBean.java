/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import business.UsersFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import model.User;

/**
 *  Adding user bean.
 * @autor Jan Tlamicha
 */
@Named
@ManagedBean
@RequestScoped
public class AddUserBean {
    
    @EJB
    UsersFacade usersfacade;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String username;
    /**
     * Plain text is not good but sufficient now
     */
    private String password;
    /**
     * Firstname of user
     */
   private String firstname;
   /**
    * Surname of user
    */
    private String surname;
    /**
     * Description of user
     */
    private String description;

    /**
     * Creates a new instance of AddBean
     */
    public AddUserBean() {
    }

     public String store() {
        usersfacade.createUser(new User(username, password, firstname, surname, description));
        return "index?faces-redirect=true";
    }

   
   
}
