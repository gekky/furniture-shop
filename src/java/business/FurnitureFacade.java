/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import integration.FurnitureDAO;
import java.util.Collection;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Furniture;

/**
 *  Facade for furniture
 * @author Jan Tlamicha
 */
@Stateless
public class FurnitureFacade {

    @Inject
//    @CustomerDAOQualifier
    FurnitureDAO furnitureDAO;
    /**
     * Furniture creation
     * @param furniture 
     */
    public void createFurniture(Furniture furniture) {
        furnitureDAO.create(furniture);
    }
    /**
     * All furniture
     * @return 
     */
    public Collection<Furniture> findAll() {
        return furnitureDAO.findAll();
    }
    /**
     * Deletion of furniture
     * @param furnitureId  ID of furniture
     */
    public void deleteFurniture(Long furnitureId) {
        furnitureDAO.delete(furnitureId);
    }
    
    public Furniture find(String id){
        return furnitureDAO.find(id);
    }

}
