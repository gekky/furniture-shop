/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business;

import integration.OrderDAO;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Ordered;

/**
 * Facade for order
 * CDI usage
 * @author jonny
 */
@Stateless
public class OrderFacade {
    @Inject
    OrderDAO ordered;

    public OrderFacade() {
    }
    /**
     * Creating order
     * @param o Order
     */
    public void create(Ordered o){
        ordered.create(o);
    }
    /**
     * Deleting order by ID of order which is searched with DAO
     * @param id 
     */
    public void delete(String id){
        ordered.delete(id);
    }
    /**
     * Finding order
     * @param id Id of the order searched with DAO
     * @return Order
     */
    public Ordered find(String id){
        return ordered.find(id);
    }
    /**
     * Collection of all orders
     * @return All orders
     */
    public Collection<Ordered> findall(){
        return ordered.findall();
    }
    
    
    
    
}
