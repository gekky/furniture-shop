/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import integration.UserDAO;
import java.util.Collection;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.User;

/**
 * Facade to controll UserDAOJPA
 * @author Jan Tlamicha
 */
@Stateless
public class UsersFacade {

    @Inject
//    @CustomerDAOQualifier
    UserDAO usersDAO;
    /**
     * Creating user
     * @param customer 
     */
    public void createUser(User customer) {
        usersDAO.create(customer);
    }
    /**
     * All users
     * @return 
     */
    public Collection<User> findAllUsers() {
        return usersDAO.findAll();
    }
    /**
     * Deleting user by his ID
     * @param username Id of user 
     */
    public void deleteUser(String username) {
        usersDAO.delete(username);
    }
    /**
     * Find user 
     * @param username
     * @return 
     */
    public User findUser(String username){
        return usersDAO.find(username);
    }

}
