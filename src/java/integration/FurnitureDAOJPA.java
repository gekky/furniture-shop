package integration;

import java.util.Collection;
import javax.ejb.Stateless;

import javax.persistence.*;
import model.Furniture;
import model.User;
@Stateless
/**
 * Implementace FurnitureDAO pro JPA
 * 
 * @author jonny
 */
public class FurnitureDAOJPA implements FurnitureDAO
{
        @PersistenceContext(name = "SemestralkaEJA-furniturePU")
        EntityManager em;

        @Override
        public void create(Furniture customer) {
            em.persist(customer);
        }

        @Override
        public Furniture find(String id) {
            return em.find(Furniture.class, id);
        }

	public FurnitureDAOJPA()
	{
	}

	FurnitureDAOJPA(EntityManager em)
	{
		this.em = em;
	}
	
	public Furniture modify(Furniture furniture)
	{
		em.find(User.class, furniture.getFurnitureID());
		Furniture mergedFurniture = em.merge(furniture);
		return mergedFurniture;
	}

	
	public void delete(Furniture furniture)
	{
		Furniture mergedUser = em.merge(furniture);
		em.remove(mergedUser);
	}

	
	public Furniture findById(String furnitureId)
	{
		Furniture furniture = em.find(Furniture.class, furnitureId);
		return furniture;
	}

	
        @Override
	public Collection<Furniture> findAll()
	{
		TypedQuery<Furniture> findAllUsersQuery = (TypedQuery < Furniture >) em.createQuery("SELECT f FROM Furniture f");
		return (Collection<Furniture>) findAllUsersQuery.getResultList();
	}


        @Override
        public void delete(Long furnitureId) {
            Furniture f = em.find(Furniture.class, furnitureId);
            em.remove(f);
        }

}
