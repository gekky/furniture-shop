/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integration;

import java.util.Collection;
import model.Ordered;

/**
 * Business interface for Orders
 * @author jonny
 */
public interface OrderDAO {
    /**
     * Creation of order
     * @param o instance of order
     */
    void create(Ordered o);
    /**
     * Deleting by ID
     * @param id  id od order
     */
    void delete(String id);
    /**
     * Finding order
     * @param id OrderedID
     * @return Order instance
     */
    Ordered find(String id);
    /**
     * Get collection of all orders
     * @return all orders
     */
    Collection<Ordered> findall();
}
