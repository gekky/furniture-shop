package integration;

import java.util.Collection;
import javax.ejb.Stateless;

import javax.persistence.*;
import model.User;
@Stateless
/**
 * Implementace UserDAO pro JPA
 * 
 * @author jonny
 */
public class UserDAOJPA implements UserDAO
{
	@PersistenceContext(name = "SemestralkaEJA-furniturePU")
	private EntityManager em;

	public UserDAOJPA()
	{
	}

	UserDAOJPA(EntityManager em)
	{
		this.em = em;
	}

	
        @Override
	public User create(User user)
	{
		em.persist(user);
		return user;
	}

	
        @Override
	public User modify(User user)
	{
		em.find(User.class, user.getUsername());
		User mergedUser = em.merge(user);
		return mergedUser;
	}

	
	public void delete(User user)
	{
		User mergedUser = em.merge(user);
		em.remove(mergedUser);
	}

	
        @Override
	public User find(String username)
	{
		User user = em.find(User.class, username);
		return user;
	}

	
        @Override
	public Collection<User> findAll()
	{
            TypedQuery<User> findAllUsersQuery = (TypedQuery<User>) em.createQuery("SELECT u FROM User u");
		return (Collection<User>)findAllUsersQuery.getResultList();
	}

        @Override
        public void delete(String username) {
            User u = em.find(User.class, username);
            em.remove(u);
        }


}
