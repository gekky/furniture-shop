/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integration;

import java.util.Collection;
import model.Furniture;

/**
 * Business Interface for furniture 
 * @author jonny
 */
public interface FurnitureDAO {
    /**
     * Creation of furniture
     * @param f Furniture
     */
    void create(Furniture f);
    /**
     * Search of furniutre
     * @param furnitureID
     * @return Furniture
     */
    Furniture find(String furnitureID);
    /**
     * Deleting furniture
     * @param furnitureID 
     */
    void delete(Long furnitureID);
    /**
     * Find all furniture
     * @return Collection of Furniture
     */
    Collection<Furniture> findAll();
    
    
}
