/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integration;

import java.util.Collection;
import model.User;

/**
 * Business interface of User
 * @author jonny
 */
public interface UserDAO {
   /**
    * Creating user
    * @param u
    * @return 
    */
    User create(User u);
    /**
     * Finding user
     * @param username
     * @return 
     */
    User find(String username);
    /**
     * Deleting user
     * @param username 
     */
    void delete(String username);
    /**
     * Returning of collection all users
     * @return 
     */
    Collection<User> findAll();
    /**
     * Merging user in matter of change
     * @param u
     * @return 
     */
    User modify(User u);
    
}
