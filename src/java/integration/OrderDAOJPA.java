/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integration;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import model.Ordered;

/**
 * Implementace OrderDAO pro JPA
 * 
 * @author jonny
 */
@Stateless
public class OrderDAOJPA implements OrderDAO{
    
    @PersistenceContext(name = "SemestralkaEJA-furniturePU")
    EntityManager em;

    @Override
    public void create(Ordered o) {
        em.persist(o);
    }

    @Override
    public void delete(String id) {
        Ordered o = em.find(Ordered.class, id);
        em.remove(o);
    }

    @Override
    public Ordered find(String id) {
        return  em.find(Ordered.class, id);
    }

    @Override
    public Collection<Ordered> findall() {
        TypedQuery<Ordered> t = (TypedQuery < Ordered >)em.createQuery("SELECT o FROM ORDERED o");
        return t.getResultList();
    }
    
}
