package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the USERS database table.
 * 
 */
@Entity
/**
 * User persistence background
 */
@Table(name = "Users")
public class User implements Serializable{
        @Id
        private String username;
        /**
         * Plain text is not good but sufficient now
         */
        private String password;
        /**
         * Firstname of user
         */
       private String firstname;
       /**
        * Surname of user
        */
        private String surname;
        /**
         * Description of user
         */
        private String description;
    /**
     * Validation
     * @return 
     */
    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }
        private boolean validated = false;

    public User() {
    }

    public User(String username, String password, String firstname, String surname, String description) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.surname = surname;
        this.description = description;
    }
    
    


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
        

	/**
	 * Returns the password of the user.
	 * 
	 * @return The user's password
	 */
	public String getPassword()
	{
		return this.password;
	}

	/**
	 * Sets the password of the user. This can be used to set the user's
	 * password in plain text. It will later be reset to a password digest by
	 * {@link UserService#modifyPassword(info.galleria.service.ejb.ModifyPasswordRequest)}
	 * .
	 * 
	 * @param password
	 *            The user's password.
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}
        
        public String getName(){
            return firstname + " " + surname;
        }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}