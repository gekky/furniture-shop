/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entity class of order
 * @author jonny
 */
@Entity
@Table(name = "Ordered")
public class Ordered implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    /**
     * Identificator of order
     */
    private Long id;
    @ManyToMany(mappedBy = "orders")
    /**
     * Collection of furniture in order
     */
    private Collection<Furniture> furniture;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ordered() {
        furniture = new LinkedList<>();
    }

    public Ordered(Collection<Furniture> furniture) {
        this.furniture = furniture;
    }
    /**
     * Adding furniture to order
     * @param f Furniture
     */
    public void addFurniture(Furniture f){
        furniture.add(f);
    }

    public Collection<Furniture> getFurniture() {
        return furniture;
    }

    public void setFurniture(Collection<Furniture> furniture) {
        this.furniture = furniture;
    }
    public void removeFurniture(String furnitureID){
        for(Furniture f : furniture){
            if(f.getFurnitureID().equals(furnitureID)){
                furniture.remove(f);
                break;
            }
        }
    }
    /**
     * Clear all furniture
     */
    public void clearFurniture(){
        furniture.clear();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordered)) {
            return false;
        }
        Ordered other = (Ordered) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Order[ id=" + id + " ]";
    }
    
}
