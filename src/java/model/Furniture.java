package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "furniture")
/**
 * Entity class for Furniture
 */
public class Furniture implements Serializable
{
        /**
         * Identificator of furniture - Unique
         */
        @Id @GeneratedValue
        protected String furnitureID;
        /**
         * Name of furniture
         */
        protected String name;
        /**
         * Price of furniture
         */
        protected double price;
        protected String description;
        protected String fotourl;
        @ManyToMany
        Collection<Ordered> orders;

    public String getFotourl() {
        return fotourl;
    }

    public void setFotourl(String fotourl) {
        this.fotourl = fotourl;
    }
        

    public Furniture() {
    }

    public Furniture(String name, double price, String description, String fotourl) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.fotourl = fotourl;
    }

    public String getFurnitureID() {
        return furnitureID;
    }

    public void setFurnitureID(String furnitureID) {
        this.furnitureID = furnitureID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

        

}